Vue.component('counter',{
    template:`
    <div>
        <h3>{{numerito}}</h3>
        <button @click="numerito++">+</button>
    </div>`,
    data(){
        return {
            numerito:0
        }
    }
})