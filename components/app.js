//componente forever alone con template script y style
Vue.component('hello',{
    //al usar un template siemple debe estar envuelto en un solo div
    template:`
      <div>
        <h4> {{hello}} </h4>
        <h6>info info</h6>
      </div>`,  
    //data en los componentes es diferente a la instancia
    data(){
        return {
            hello: 'componente holi'
        }
    }
});


